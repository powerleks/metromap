$(document).ready(function() {

  var stations = [{"name": "Девяткино", "lineId": "1", "id": "1", "color": "#EF1E25"}, {"name": "Гражданский проспект", "lineId": "1", "id": "2", "color": "#EF1E25"}, {"name": "Академическая", "lineId": "1", "id": "3", "color": "#EF1E25"}, {"name": "Политехническая", "lineId": "1", "id": "4", "color": "#EF1E25"}, {"name": "Площадь Мужества", "lineId": "1", "id": "5", "color": "#EF1E25"}, {"name": "Лесная", "lineId": "1", "id": "6", "color": "#EF1E25"}, {"name": "Выборгская", "lineId": "1", "id": "7", "color": "#EF1E25"}, {"name": "Площадь Ленина", "lineId": "1", "id": "8", "color": "#EF1E25"}, {"name": "Чернышевская", "lineId": "1", "id": "9", "color": "#EF1E25"}, {"name": "Площадь Восстания", "lineId": "1", "id": "10", "color": "#EF1E25"}, {"name": "Владимирская", "lineId": "1", "id": "11", "color": "#EF1E25"}, {"name": "Пушкинская", "lineId": "1", "id": "12", "color": "#EF1E25"}, {"name": "Технологический институт - 1", "lineId": "1", "id": "13", "color": "#EF1E25"}, {"name": "Балтийская", "lineId": "1", "id": "14", "color": "#EF1E25"}, {"name": "Нарвская", "lineId": "1", "id": "15", "color": "#EF1E25"}, {"name": "Кировский завод", "lineId": "1", "id": "16", "color": "#EF1E25"}, {"name": "Автово", "lineId": "1", "id": "17", "color": "#EF1E25"}, {"name": "Ленинский проспект", "lineId": "1", "id": "18", "color": "#EF1E25"}, {"name": "Проспект Ветеранов", "lineId": "1", "id": "19", "color": "#EF1E25"}, {"name": "Парнас", "lineId": "2", "id": "20", "color": "#019EE0"}, {"name": "Проспект Просвещения", "lineId": "2", "id": "21", "color": "#019EE0"}, {"name": "Озерки", "lineId": "2", "id": "22", "color": "#019EE0"}, {"name": "Удельная", "lineId": "2", "id": "23", "color": "#019EE0"}, {"name": "Пионерская", "lineId": "2", "id": "24", "color": "#019EE0"}, {"name": "Чёрная речка", "lineId": "2", "id": "25", "color": "#019EE0"}, {"name": "Петроградская", "lineId": "2", "id": "26", "color": "#019EE0"}, {"name": "Горьковская", "lineId": "2", "id": "27", "color": "#019EE0"}, {"name": "Невский проспект", "lineId": "2", "id": "28", "color": "#019EE0"}, {"name": "Сенная площадь", "lineId": "2", "id": "29", "color": "#019EE0"}, {"name": "Технологический институт - 2", "lineId": "2", "id": "30", "color": "#019EE0"}, {"name": "Фрунзенская", "lineId": "2", "id": "31", "color": "#019EE0"}, {"name": "Московские ворота", "lineId": "2", "id": "32", "color": "#019EE0"}, {"name": "Электросила", "lineId": "2", "id": "33", "color": "#019EE0"}, {"name": "Парк Победы", "lineId": "2", "id": "34", "color": "#019EE0"}, {"name": "Московская", "lineId": "2", "id": "35", "color": "#019EE0"}, {"name": "Звёздная", "lineId": "2", "id": "36", "color": "#019EE0"}, {"name": "Купчино", "lineId": "2", "id": "37", "color": "#019EE0"}, {"name": "Приморская", "lineId": "3", "id": "38", "color": "#029A55"}, {"name": "Василеостровская", "lineId": "3", "id": "39", "color": "#029A55"}, {"name": "Гостиный двор", "lineId": "3", "id": "40", "color": "#029A55"}, {"name": "Маяковская", "lineId": "3", "id": "41", "color": "#029A55"}, {"name": "Площадь Александра Невского - 1", "lineId": "3", "id": "42", "color": "#029A55"}, {"name": "Елизаровская", "lineId": "3", "id": "43", "color": "#029A55"}, {"name": "Ломоносовская", "lineId": "3", "id": "44", "color": "#029A55"}, {"name": "Пролетарская", "lineId": "3", "id": "45", "color": "#029A55"}, {"name": "Обухово", "lineId": "3", "id": "46", "color": "#029A55"}, {"name": "Рыбацкое", "lineId": "3", "id": "47", "color": "#029A55"}, {"name": "Спасская", "lineId": "4", "id": "48", "color": "#FBAA33"}, {"name": "Достоевская", "lineId": "4", "id": "49", "color": "#FBAA33"}, {"name": "Лиговский проспект", "lineId": "4", "id": "50", "color": "#FBAA33"}, {"name": "Площадь Александра Невского - 2", "lineId": "4", "id": "51", "color": "#FBAA33"}, {"name": "Новочеркасская", "lineId": "4", "id": "52", "color": "#FBAA33"}, {"name": "Ладожская", "lineId": "4", "id": "53", "color": "#FBAA33"}, {"name": "Проспект Большевиков", "lineId": "4", "id": "54", "color": "#FBAA33"}, {"name": "Улица Дыбенко", "lineId": "4", "id": "55", "color": "#FBAA33"}, {"name": "Комендантский проспект", "lineId": "5", "id": "56", "color": "#B61D8E"}, {"name": "Старая Деревня", "lineId": "5", "id": "57", "color": "#B61D8E"}, {"name": "Крестовский остров", "lineId": "5", "id": "58", "color": "#B61D8E"}, {"name": "Чкаловская", "lineId": "5", "id": "59", "color": "#B61D8E"}, {"name": "Спортивная", "lineId": "5", "id": "60", "color": "#B61D8E"}, {"name": "Садовая", "lineId": "5", "id": "61", "color": "#B61D8E"}, {"name": "Звенигородская", "lineId": "5", "id": "62", "color": "#B61D8E"}, {"name": "Волковская", "lineId": "5", "id": "63", "color": "#B61D8E"}, {"name": "Обводный канал", "lineId": "5", "id": "64", "color": "#B61D8E"}, {"name": "Адмиралтейская", "lineId": "5", "id": "65", "color": "#B61D8E"}, {"name": "Бухарестская", "lineId": "5", "id": "66", "color": "#B61D8E"}, {"name": "Международная", "lineId": "5", "id": "67", "color": "#B61D8E"}];
  var states = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    // `states` is an array of state names defined in "The Basics"
    local: stations
  });

  $('#from').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  },
  {
    name: 'states',
    display: 'name',
    source: states,
    templates: {
      suggestion: function (data) {
        return '<p style="color:' + data.color + '">' + data.name + '</p>';
    }
    }
  });

  $('#from').on(
      "typeahead:selected typeahead:autocompleted",
      function(e,datum) {
        $('#from_id').val(datum.id);
      }
  );

  $('#to').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  },
  {
    name: 'states',
    display: 'name',
    source: states,
    templates: {
      suggestion: function (data) {
        return '<p style="color:' + data.color + '">' + data.name + '</p>';
    }
    }
  });

  $('#to').on(
      "typeahead:selected typeahead:autocompleted",
      function(e,datum) {
        $('#to_id').val(datum.id);
      }
  );
});