import sqlite3


class DBHelper:
    def __init__(self, dbname="db.sqlite"):
        self.dbname = dbname
        self.conn = sqlite3.connect(dbname)

    def setup(self):
        stmt = "CREATE TABLE IF NOT EXISTS Edges (station1 INT, station2 INT, cost INT, city text, " \
               "PRIMARY KEY (station1, station2, city))"
        self.conn.execute(stmt)
        self.conn.commit()
        stmt = "CREATE TABLE IF NOT EXISTS Station_data (id Int, name text, city text, color text, lineId Int," \
               "PRIMARY KEY (id, city))"
        self.conn.execute(stmt)
        self.conn.commit()

    def add_station_data(self, id, name, city, color, lineId):
        stmt = "INSERT INTO Station_data (id, name, city, color, lineId) VALUES (?,?,?,?,?)"
        args = (id, name, city, color, lineId)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def add_edge(self, frm, to, cost, city):
        stmt = "INSERT INTO Edges (station1, station2, cost, city) VALUES (?,?,?,?)"
        args = (frm, to, cost, city)
        self.conn.execute(stmt, args)
        self.conn.commit()


    def get_edges(self, city):
        stmt = "SELECT station1, station2, cost FROM Edges WHERE city = (?)"
        args = (city, )
        edges = [x for x in self.conn.execute(stmt, args)]
        return edges

    def get_station_data(self, city):
        stmt = "SELECT * FROM Station_data WHERE city = (?)"
        args = (city, )
        station_data = [x for x in self.conn.execute(stmt, args)]
        keys = ('id', 'name', 'city', 'color', 'lineId')
        d = {}
        for station in station_data:
            d[station[0]] = dict(zip(keys, station))
        return d