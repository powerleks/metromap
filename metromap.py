from flask import Flask, redirect, render_template, request, url_for

from find_path import find_route

app = Flask(__name__)


@app.route('/')
def home():
    return redirect(url_for('city', city='moscow'))

@app.route('/<string:city>/')
def city(city):
    return render_template('index.html', city=city, priority='time')

@app.route('/<string:city>/path')
def build_path(city):
    from_id = request.args.get('from')
    to_id = request.args.get('to')
    priority = request.args.get('priority')
    if from_id and to_id:
        cost, transfers, path = find_route(from_id, to_id, city, priority)
        return render_template('index.html', path=path, cost=cost, transfers=transfers,
                               frm=from_id, to=to_id, city=city, priority=priority)
    else:
        return render_template('index.html', city=city, priority='time')

if __name__ == '__main__':
    app.run()
