import os
from dbhelper import DBHelper
from collections import defaultdict
from heapq import heappush, heappop

SITE_ROOT = os.path.realpath(os.path.dirname(__file__))

db_url = os.path.join(SITE_ROOT, "db.sqlite")
db = DBHelper(db_url)
db.setup()

def dijkstra(edges, frm, to):
    g = defaultdict(list)
    for l, r, c in edges:
        g[l].append((c,r))
        g[r].append((c,l))

    heap, seen = [(0,frm,())], set()
    while heap:
        (cost,v1,path) = heappop(heap)

        if v1 not in seen:
            seen.add(v1)
            path = (v1, path)
            if v1 == to:
                return (cost, path)

            for c, v2 in g.get(v1, ()):
                if v2 not in seen:
                    heappush(heap, (cost+c, v2, path))
      
    return None, None

def add_penalty(edges, city, time=5):
    station_data = db.get_station_data(city)

    new_edges = []
    for i, (l, r, c) in enumerate(edges):
        if station_data[l]['lineId'] != station_data[r]['lineId']:
            c += time
        new_edges.append((l, r, c))
    return new_edges

def remove_penalty(cost, route, city, time=5):
    station_data = db.get_station_data(city)

    prev_lineId = station_data[route[0]]['lineId']
    for station in route[1:]:
        if station_data[station]['lineId'] != prev_lineId:
            cost -= time
        prev_lineId = station_data[station]['lineId']
    return cost

def count_transers(route):
    transers = 0
    prev_lineId = route[0]['lineId']
    for station in route:
        if station['lineId'] != prev_lineId:
            transers += 1
        prev_lineId = station['lineId']
    return transers

def find_route(frm, to, city, priority='time'):
    frm = int(frm)
    to = int(to)
    edges = db.get_edges(city)

    if priority == 'less_transfers':
        edges = add_penalty(edges, city)
    else:
        edges = edges

    cost, path = dijkstra(edges, frm, to)

    if not cost:
        return (None, [])

    route = []
    station = path
    while station:
        route.append(station[0])
        station = station[1]

    if priority == 'less_transfers':
        cost = remove_penalty(cost, route, city)

    station_data = db.get_station_data(city)

    route.reverse()
    route = list(map(lambda x: station_data[x], route))
    transers = count_transers(route)
    return (cost, transers, route)
